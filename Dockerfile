# multistage: 1
FROM golang:alpine AS builder
RUN apk --no-cache add git && \
    git clone https://github.com/awslabs/amazon-ecr-credential-helper /go/src/github.com/awslabs/amazon-ecr-credential-helper && \
    go build -o /assets/docker-credential-ecr-login github.com/awslabs/amazon-ecr-credential-helper/ecr-login/cli/docker-credential-ecr-login


# mutlistage: 2
FROM openjdk:8-jdk-alpine AS resource
COPY --from=builder /assets/docker-credential-ecr-login /usr/local/bin/docker-credential-ecr-login
